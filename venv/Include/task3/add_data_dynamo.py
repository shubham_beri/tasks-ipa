from clients_n_resource import dynamodb_resource

class AddDataToDynamo:
    def __init__(self, table_name):
        self.table1 = dynamodb_resource.Table(table_name)

        self.table1.put_item(
        Item={
            'FileType': 'csv',
            'RequiredJob': 'Firs Glue Job'
        })


        self.table1.put_item(
        Item={
            'FileType': 'py',
            'RequiredJob': 'Second Glue Job'
       })