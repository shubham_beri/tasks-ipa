import boto3

s3_client = boto3.client('s3', region_name= 'us-east-2')
cf_client = boto3.client('cloudformation')
dynamodb_resource = boto3.resource('dynamodb')