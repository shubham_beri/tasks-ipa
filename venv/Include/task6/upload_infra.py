import botocore
from clients import cf_client
from config import stack_name



class DeployInfra:
    def __init__(self, stack_name):
        self.stack_name = stack_name
        self.cf_client = cf_client
        print(12)

    def parse_template(self,template):
        with open(template) as template_fileobj:
            template_data = template_fileobj.read()
        try:
            self.cf_client.validate_template(TemplateBody=template_data)

            return template_data
        except botocore.exceptions.ClientError as e:
            print(e)



    def create_update_infra(self):

        try:

            response = self.cf_client.create_stack(
                StackName= self.stack_name,
                TemplateBody= self.parse_template('infra.yaml'),
                Capabilities=[
                    'CAPABILITY_IAM',  'CAPABILITY_NAMED_IAM'
                ])



            self.cf_client.get_waiter('stack_create_complete').wait(StackName=self.stack_name)
        except botocore.exceptions.ClientError as e:
            if(e.response['Error']['Code'] == 'AlreadyExistsException'):
                try:
                    print('ki')
                    self.cf_client.update_stack(
                    StackName= stack_name,
                    TemplateBody= self.parse_template('infra.yaml'),
                    Capabilities=[
                        'CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM'
                    ])
                    self.cf_client.get_waiter('stack_update_complete').wait(StackName=self.stack_name)
                except Exception as e:
                    print('No updates to be performed.', e)
            else:
                print(e)

        except botocore.exceptions.ParamValidationError as e:
            print("Error in template.")








df_obj = DeployInfra(stack_name)
print(type(df_obj))
df_obj.create_update_infra()

