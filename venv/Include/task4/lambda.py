import json
import pymysql
import boto3


class rds_db:
    def __init__(self, ENDPOINT, USR, token, PORT, DBNAME):
        try:
            self.conn = pymysql.connect(host=ENDPOINT, user=USR, passwd=token, port=PORT, database=DBNAME)
            self.cur = self.conn.cursor()
        except Exception as e:
            print("Database connection failed due to {}".format(e))

    def create_table(self, query):
        self.cur.execute(query)
        self.conn.commit()

    def add_row(self, query):
        self.cur.execute(query)
        self.conn.commit()

    def fetch_rows(self, table_name):
        query = 'SELECT * FROM {}'.format(table_name)
        self.cur.execute(query)
        result = self.cur.fetchall();
        print('Res::::', result)
        return result

    def login_verify(self, input_customer_id, input_customer_pwd):
        print(type(input_customer_id))
        print(input_customer_id)
        query = "SELECT C_PWD FROM {} WHERE C_ID = '{}'".format('CUSTOMER', input_customer_id)

        print(query)
        self.cur.execute(query)
        result = self.cur.fetchall();
        actual_pwd = result[0][0]

        if actual_pwd == input_customer_pwd:
            print('Hello')
            return True
        else:
            return False


def lambda_handler(event, context):
    # TODO implement

    rds_client = boto3.client('rds')

    try:
        dbs = rds_client.describe_db_instances()
        db = dbs['DBInstances'][0]
        USR = db['MasterUsername']
        ENDPOINT = db['Endpoint']['Address']
        PORT = db['Endpoint']['Port']
        DBNAME = 'mydb'
        REGION = "us-east-2b"
        PWD = 'Vs86z8tOanWk7oCO9xKD'


    except Exception as e:
        print(e)

    db_obj = rds_db(ENDPOINT, USR, PWD, PORT, DBNAME)

    #    db_obj.create_table(sql2)
    #    db_obj.add_row(sql3)
    db_obj.fetch_rows('CUSTOMER')
    # db_obj.fetch_rows('ACCOUNT')
    return_obj = []
    return_obj.append(db_obj.fetch_rows('ACCOUNT'))
    print(event["queryStringParameters"], 'loppp')
    input_customer_id = event["queryStringParameters"]["userId"]
    input_customer_pwd = event["queryStringParameters"]["password"]

    resObject = {}
    resObject['statusCode'] = 200
    resObject['headers'] = {}
    resObject['headers']['Content-type'] = 'application/json'

    login_or_not = db_obj.login_verify(input_customer_id, input_customer_pwd);
    if (login_or_not):
        resObject['body'] = json.dumps('0')

    else:
        resObject['body'] = json.dumps('1')

    print(resObject)
    return resObject

