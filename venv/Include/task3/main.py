from config import code_bucket, job_code_bucket, stack_name, table_name
from create_zip_upload import CreateZipUpload
from deploy_infra import DeployInfra
from final_trigger import FinalTrigger
from add_data_dynamo import AddDataToDynamo


#Creating all the prerequisites resources and putting in objects
create_zip_upload_obj = CreateZipUpload()
create_zip_upload_obj.create_zip_file()
create_zip_upload_obj.create_code_bucket(code_bucket)
create_zip_upload_obj.upload_lambda_zip_file(code_bucket)


#Deploying the infrastructure through CloudFormation
deploy_infra_obj = DeployInfra(stack_name)
deploy_infra_obj.create_update_infra()

#upload glue job here because job bucket was in yaml file
create_zip_upload_obj.upload_glue_job_file(job_code_bucket)


#Adding data to DynamoDB Table
add_data_dynamo_obj = AddDataToDynamo(table_name)

#Triggering Lambda Function
final_trigger_obj = FinalTrigger()
final_trigger_obj.upload_csv_file_for_triggering()
