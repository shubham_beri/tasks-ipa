from init import dynamo_resource, table_name, glue_client, athena_client
import time


def lambda_handler(event, context):
    crawler = 'testcrawlerone'
    crawler2 = 'testcrawler2'
    QueryString = 'SELECT * FROM "dbcrawler"."destination_job_result_195" limit 10;'
    QueryExecutionContext = {'Database': 'dbcrawler'}
    ResultConfiguration = {'OutputLocation': 's3://final-result-bucket-195/'}

    source_bucket_object_key = event['Records'][0]['s3']['object']['key']
    file_type = source_bucket_object_key.split('.')[1]

    table1 = dynamo_resource.Table(table_name)
    dynamo_res = table1.get_item(
        Key={
            'FileType': file_type
        }
    )

    required_job_name = dynamo_res['Item']['RequiredJob']
    print('Required JobName: ', required_job_name)

    if start_crawler_check(crawler):
        try:
            glue_client.start_crawler(Name=crawler)
            wait_until_crawler_stops(crawler)
            start_job_response = start_gluejob(required_job_name)
            job_run_id = start_job_response['JobRunId']
            if wait_until_job_over(job_run_id, required_job_name):
                print("Glue Run Successful")
                if start_crawler_check(crawler2):
                    try:
                        glue_client.start_crawler(Name=crawler2)
                        wait_until_crawler_stops(crawler2)
                        run_athena_query(QueryString, QueryExecutionContext, ResultConfiguration)
                    except e:
                        print('Error is: ', e)
                else:
                    print("Crawler is not ready to start.")
            else:
                print("Some error was encountered while running glue job.")
        except e:
            print('Error is', e)

    else:
        print("Not able to start")


def start_crawler_check(crawler_name):
    print('Inside start crawler check')
    crawler_res = glue_client.get_crawler(Name=crawler_name)
    curr_crawler_state = (crawler_res['Crawler']['State'])
    if curr_crawler_state == 'READY':
        return True
    else:
        print(curr_crawler_state)
        return False


def wait_until_crawler_stops(crawler_name):
    while True:
        print('Inside while loop of wait_until_crawler_stops')
        crawler_res = glue_client.get_crawler(Name=crawler_name)
        curr_crawler_state = (crawler_res['Crawler']['State'])
        print(curr_crawler_state)
        if (curr_crawler_state) == 'READY':
            break
        time.sleep(15)


def start_gluejob(job_name):
    return glue_client.start_job_run(JobName=job_name)


def wait_until_job_over(job_run_id, job_name):
    while True:
        get_job_response = glue_client.get_job_run(
            JobName=job_name,
            RunId=job_run_id)
        curr_job_state = (get_job_response['JobRun']['JobRunState'])
        if curr_job_state == 'SUCCEEDED':
            return True

        else:
            time.sleep(30)


def run_athena_query(QueryString, QueryExecutionContext, ResultConfiguration):
    athena_response = athena_client.start_query_execution(
        QueryString=QueryString,
        QueryExecutionContext=QueryExecutionContext,
        ResultConfiguration=ResultConfiguration
    )

    return athena_response