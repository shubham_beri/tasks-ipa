import boto3
dynamo_resource = boto3.resource('dynamodb')
table_name = 'MyDynamoTable'
glue_client = boto3.client('glue')
athena_client = boto3.client('athena')
