import task2
import boto3
import botocore

# Get the service resource.

table = task2.dynamodb.Table('users')
try:
    task2.createTable('users')
except botocore.exceptions.ClientError as e:
    print('Table cannot be created it already exists')

res = task2.batchWrite('users')
