import botocore
from clients import cf_client
from config import stack_name

def parse_template(template):
    with open(template) as template_fileobj:
        template_data = template_fileobj.read()
    try:
        cf_client.validate_template(TemplateBody=template_data)
        return template_data
    except botocore.exceptions.ClientError as e:
        print(e)

try:
    response = cf_client.create_stack(
        StackName= stack_name,
        TemplateBody= parse_template('infra.yaml'),
        Capabilities=[
            'CAPABILITY_IAM',  'CAPABILITY_NAMED_IAM'
        ])

    cf_client.get_waiter('stack_create_complete').wait(StackName=stack_name)
except botocore.exceptions.ClientError as e:
    if(e.response['Error']['Code'] == 'AlreadyExistsException'):
        try:
            cf_client.update_stack(
            StackName= stack_name,
            TemplateBody=parse_template('infra.yaml'),
            Capabilities=[
                'CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM'
            ])
            cf_client.get_waiter('stack_update_complete').wait(StackName=stack_name)
        except:
            print('No updates to be performed.')

except botocore.exceptions.ParamValidationError as e:
    print("Error in template.")





