from init import destination_bucket, s3_resource

def lambda_handler(event, context):
    source_bucket_name = event['Records'][0]['s3']['bucket']['name']
    source_bucket_object_key = event['Records'][0]['s3']['object']['key']
    copy_source = {
        'Bucket': source_bucket_name,
        'Key': source_bucket_object_key
    }
    bucket = s3_resource.Bucket(destination_bucket)
    bucket.copy(copy_source, source_bucket_object_key)


