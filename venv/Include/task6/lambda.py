from init import s3_client
import csv
import json
import codecs


def convert_xml_row(row):
    return """
    <name>%s</name>
    <address>%s</address>
    <dob>%s</dob>
    <age>%s</age>""" % (row[0], row[1], row[2], row[3])


def return_python_object(source_bucket_name, source_bucket_object_key):
    list_data_objects = []
    response = s3_client.get_object(Bucket=source_bucket_name, Key=source_bucket_object_key)
    for row in csv.reader(codecs.getreader("utf-8")(response["Body"])):
        list_data_objects.append(row)

    return list_data_objects


def csv_to_json(source_bucket_name, source_bucket_object_key):
    list_data_objects = []
    response = s3_client.get_object(Bucket=source_bucket_name, Key=source_bucket_object_key)
    for row in csv.DictReader(codecs.getreader("utf-8")(response["Body"])):
        list_data_objects.append(row)

    return list_data_objects


def lambda_handler(event, context):
    source_bucket_name = event['Records'][0]['s3']['bucket']['name']
    source_bucket_object_key = event['Records'][0]['s3']['object']['key']
    copy_source = {
        'Bucket': source_bucket_name,
        'Key': source_bucket_object_key
    }

    jsonResponse = csv_to_json(source_bucket_name, source_bucket_object_key)
    s3_client.put_object(Body=json.dumps(jsonResponse), Bucket=source_bucket_name, Key='newJsonSample.json')
    print(jsonResponse)

    data = return_python_object(source_bucket_name, source_bucket_object_key)
    xml_data = ('\n'.join([convert_xml_row(row) for row in data[1:]]))

    final_xml_data = '<data>' + xml_data + '\n</data>'
    print(final_xml_data)
    s3_client.put_object(Body=final_xml_data, Bucket=source_bucket_name, Key='newXMLSample.xml')

