from config import source_bucket
import os
from file_upload_to_s3 import upload_file


class FinalTrigger:
    def upload_csv_file_for_triggering(self):
        for file in os.listdir():
            if 'Book1.csv' in file:
                upload_file(source_bucket, file)