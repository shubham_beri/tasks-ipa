import csv
import json
import os


def make_json(csvFilePath, jsonFilePath):


    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)
        list_data_objects = []


        for rows in csvReader:
            list_data_objects.append(rows)

        with open(jsonFilePath, 'a', encoding='utf-8') as jsonf:
            jsonf.write(json.dumps(list_data_objects, indent=4))


csvFilePath = 'sample.csv'
jsonFilePath = 'newJsonSample.json'


make_json(csvFilePath, jsonFilePath)
