from moto import mock_dynamodb2
from task2 import Crud
import unittest


class test_task2(unittest.TestCase):
    @mock_dynamodb2
    def test_addRow(self):
        crud_obj = Crud()
        crud_obj.createTable('test12')
        res = crud_obj.addRow('E234', 'Rammanohar Joshi', 54, 'test12')
        try:
            self.assertEqual(200, res['ResponseMetadata']['HTTPStatusCode'])
            print('Test for addRow Success!')
        except:
            print('Test Case For method addRow() Failed.')

    @mock_dynamodb2
    def test_retrieveRow(self):
        crud_obj = Crud()
        crud_obj.createTable('test12')
        res = crud_obj.addRow('E234', 'Rammanohar Joshi', 54, 'test12')
        retrieve = crud_obj.retrieveRow('E234','test12')
        expectedResult = {'emp_id': 'E234', 'full_name': 'Rammanohar Joshi', 'age': 54}
        try:
            self.assertEqual(retrieve,expectedResult)
            print('Test for retrieveRow Success!')
        except:
            print('Test Case For Method retrieveRow Failed')

    @mock_dynamodb2
    def test_updateRow(self):
        crud_obj = Crud()
        crud_obj.createTable('test12')
        res = crud_obj.addRow('E234', 'Rammanohar Joshi', 54, 'test12')
        update = crud_obj.updateAge('E234', 78, 'test12')
        retrieveUpdated = crud_obj.retrieveRow('E234','test12')

        expectedResult2 = {'emp_id': 'E234', 'full_name': 'Rammanohar Joshi', 'age': 78}
        try:
            self.assertEqual(retrieveUpdated, expectedResult2)
            print('Test for updateAge Success!')
        except:
            print('Test Case For Method updateAge Failed')

    @mock_dynamodb2
    def test_deleteRow(self):
        crud_obj = Crud()
        crud_obj.createTable('test12')
        res = crud_obj.addRow('E234', 'Rammanohar Joshi', 54, 'test12')
        delete = crud_obj.deleteRow('E234', 'test12')
        try:
            self.assertEqual(200, res['ResponseMetadata']['HTTPStatusCode'])
            print('Test for deleteRow Success!')
        except:
            print('Test Case For method deleteRow() Failed.')


if __name__ == '__main__':
    unittest.main()