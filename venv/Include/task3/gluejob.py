import sys
import datetime
import json
from collections import Iterable, OrderedDict
from itertools import product
import logging
import boto3
import pyspark
from pyspark.sql import Row
from pyspark.sql.column import Column, _to_java_column
from pyspark.sql.types import array, ArrayType, IntegerType, NullType, StringType, StructType
from pyspark.sql.functions import col, concat_ws, collect_list, explode, lit, split, when, upper
###################GLUE import##############
from awsglue.context import GlueContext
from awsglue.dynamicframe import DynamicFrame
from awsglue.transforms import Relationalize
from awsglue.utils import getResolvedOptions
from awsglue.job import Job
from awsglue.transforms import *
#### ###creating spark and gluecontext ###############
sc = pyspark.SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
logger = logging.getLogger(__name__)
start_dttm=datetime.datetime.now()
print("Job Execution Started…")
## ###Creating glue dynamic frame from the catalog ###########
ds = glueContext.create_dynamic_frame.from_catalog(database = "dbcrawler", table_name = "config_bucket_195", transformation_ctx = "datasource0")
##### ##Creating spark data frame from the glue context ########
ds_df = ds.toDF()
ds_df.show()
time_stamp_final = datetime.datetime.now()
ds_df.select("sno","name","city").show()
ds_df1 = ds_df.select('sno',upper(col('name')),'city')
ds_df2 = ds_df1.withColumn("Timestamp",lit(time_stamp_final))
datasource0 = DynamicFrame.fromDF(ds_df2, glueContext, "datasource0")
datasink2 = glueContext.write_dynamic_frame.from_options(frame = datasource0, connection_type = "s3", connection_options = {"path": "s3://destination-job-result-195/"},format = "json", transformation_ctx = "datasink2")
job.commit()