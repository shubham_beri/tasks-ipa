from zipfile import ZipFile
import os
import zipfile
from clients import s3_client
from config import code_bucket


#Put it in function and use classes
try:
    zip_file = zipfile.ZipFile('lambda.zip', 'w')
    zip_file.write('init.py')
    zip_file.write('lambda.py')
    zip_file.close()
except e:
    print('Error!! While Making ZipFile', e)


def upload_zip_files(bucket_name):
    for file in os.listdir():
        if '.zip' in file:
            upload_to_bucket = code_bucket
            upload_to_bucket_dir = str(file)
            s3_client.upload_file(file,upload_to_bucket,upload_to_bucket_dir)

upload_zip_files(code_bucket)