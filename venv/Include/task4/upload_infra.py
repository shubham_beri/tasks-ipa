import botocore
from clients import cf_client
from config import stack_name2



class DeployInfra:
    def __init__(self, stack_name2):
        self.stack_name2 = stack_name2
        self.cf_client = cf_client
        print(12)

    def parse_template(self,template):
        with open(template) as template_fileobj:
            template_data = template_fileobj.read()
        try:
            self.cf_client.validate_template(TemplateBody=template_data)

            return template_data
        except botocore.exceptions.ClientError as e:
            print(e)



    def create_update_infra(self):

        try:

            response = self.cf_client.create_stack(
                StackName= self.stack_name2,
                TemplateBody= self.parse_template('infra2.yaml'),
                Capabilities=[
                    'CAPABILITY_IAM',  'CAPABILITY_NAMED_IAM'
                ])



            self.cf_client.get_waiter('stack_create_complete').wait(StackName=self.stack_name2)
        except botocore.exceptions.ClientError as e:
            if(e.response['Error']['Code'] == 'AlreadyExistsException'):
                try:
                    print('ki')
                    self.cf_client.update_stack(
                    StackName= stack_name2,
                    TemplateBody= self.parse_template('infra2.yaml'),
                    Capabilities=[
                        'CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM'
                    ])
                    self.cf_client.get_waiter('stack_update_complete').wait(StackName=self.stack_name2)
                except Exception as e:
                    print('No updates to be performed.', e)
            else:
                print(e)

        except botocore.exceptions.ParamValidationError as e:
            print("Error in template.")








df_obj = DeployInfra(stack_name2)
print(type(df_obj))
df_obj.create_update_infra()

