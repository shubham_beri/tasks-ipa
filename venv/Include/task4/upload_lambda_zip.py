import os
import zipfile
from clients import s3_client


def upload_file(bucket_name, file_name):

    for file in os.listdir():
        if file_name in file:
            s3_client.upload_file(file,bucket_name, str(file_name))



def create_zip_file():
    try:
        zip_file = zipfile.ZipFile('lambda.zip', 'w')
        zip_file.write('lambda.py')
        zip_file.close()
    except Exception as e:
        print('Error!! While Making ZipFile', e)


def upload_lambda_zip_file(bucket_name):
        for file in os.listdir():
            if 'lambda.zip' in file:
                upload_file(bucket_name, file)


# create_zip_file()
upload_lambda_zip_file('new-task4-code-bucket')