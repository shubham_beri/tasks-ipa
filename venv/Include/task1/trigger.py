import boto3
import os
from clients import s3_client
from config import source_bucket

def upload_file(bucket_name):

    for file in os.listdir():
        if 'infra.yaml' in file:
            s3_client.upload_file(file,bucket_name, str(file))

upload_file(source_bucket)