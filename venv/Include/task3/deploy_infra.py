import botocore
from clients_n_resource import cf_client



class DeployInfra:
    def __init__(self, stack_name):
        self.stack_name = stack_name
        self.template = 'infra.yaml'

    def parse_template(self, template):
        with open(template) as template_fileobj:
            template_data = template_fileobj.read()
        try:
            cf_client.validate_template(TemplateBody=template_data)
            return template_data
        except botocore.exceptions.ClientError as e:
            print(e)


    def create_update_infra(self):
        try:
            response = cf_client.create_stack(
                StackName= self.stack_name,
                TemplateBody= self.parse_template(self.template),
                Capabilities=[
                    'CAPABILITY_IAM',  'CAPABILITY_NAMED_IAM'
                ])

            cf_client.get_waiter('stack_create_complete').wait(StackName=self.stack_name)
        except botocore.exceptions.ClientError as e:
            if(e.response['Error']['Code'] == 'AlreadyExistsException'):
                try:
                    cf_client.update_stack(
                    StackName= self.stack_name,
                    TemplateBody=self.parse_template(self.template),
                    Capabilities=[
                        'CAPABILITY_IAM', 'CAPABILITY_NAMED_IAM'
                    ])
                    cf_client.get_waiter('stack_update_complete').wait(StackName=self.stack_name)
                except:
                    print('No updates to be performed.')

        except botocore.exceptions.ParamValidationError as e:
            print("Error in template.")





