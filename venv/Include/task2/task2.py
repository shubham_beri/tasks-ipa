import boto3

class Crud:
    def __init__(self):
        self.conn = boto3.resource('dynamodb')


    def createTable(self, table_name):

        table1 = self.conn.create_table(
        TableName=table_name,
        KeySchema=[
            {
                'AttributeName': 'emp_id',
                'KeyType': 'HASH'
            }
        ],
        AttributeDefinitions= [{
          'AttributeName': 'emp_id',
        'AttributeType': 'S'
        }],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )

        table1.meta.client.get_waiter('table_exists').wait(TableName=table_name)


        return table1


    def addRow(self, emp_id, full_Name, age, table_name):

        table1 = self.conn.Table(table_name)
        return table1.put_item(
            Item={
                'emp_id': emp_id,
                'full_name': full_Name,
                'age': age
            }
        )

    def retrieveRow(self, emp_id, table_name):

        table1 = self.conn.Table(table_name)
        response = table1.get_item(
            Key= {
                'emp_id': emp_id,
            }
        )

        return (response['Item'])


    def updateAge(self, emp_id, newAge, table_name):

         table1 = self.conn.Table(table_name)
         return table1.update_item(
                Key={
                    'emp_id': emp_id,
                },
                UpdateExpression='SET age = :val1',
                ExpressionAttributeValues={
                    ':val1': newAge
                }
            )

    def deleteRow(self, emp_id, table_name):

        return self.conn.Table(table_name).delete_item(
                Key={
                    'emp_id': emp_id,
                }
            )

    def batchWrite(self, table_name):
        table = self.conn.Table(table_name)
        with table.batch_writer() as batch:
            for i in range(50):
                batch.put_item(
                    Item={
                        'emp_id': 'E' + str(i),
                    }
                )




# crud_obj = Crud()
# #crud_obj.createTable('Employee')
# crud_obj.addRow('E101','Sudhakar Kashyap', 34, 'Employee')