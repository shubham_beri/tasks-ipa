from zipfile import ZipFile
import os
import zipfile
from clients_n_resource import s3_client
from file_upload_to_s3 import upload_file


#Put it in function and use classes
class CreateZipUpload:
    def create_zip_file(self):
        try:
            zip_file = zipfile.ZipFile('lambda.zip', 'w')
            zip_file.write('init.py')
            zip_file.write('lambda.py')
            zip_file.close()
        except e:
            print('Error!! While Making ZipFile', e)

    def create_code_bucket(self,bucket_name):
        region = 'us-east-2'
        location = {'LocationConstraint': region}

        s3_client.create_bucket(Bucket=bucket_name, CreateBucketConfiguration=location)
        s3_client.get_waiter('bucket_exists').wait(Bucket=bucket_name)

    def upload_lambda_zip_file(self,bucket_name):
        for file in os.listdir():
            if 'lambda.zip' in file:
                upload_file(bucket_name, file)

    def upload_glue_job_file(self, job_bucket):
        for file in os.listdir():
            if 'gluejob.py' in file:
                upload_file(job_bucket, file)



