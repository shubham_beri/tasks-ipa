import os
import csv

def return_python_object(csvFilePath):
    with open(csvFilePath, encoding='utf-8') as csvf:
        csvReader = csv.reader(csvf)
        list_data_objects = []
        for rows in csvReader:
            list_data_objects.append(rows)

    return (list_data_objects)

def convert_xml_row(row):
    return """
    <name>%s</name>
    <address>%s</address>
    <dob>%s</dob>
    <age>%s</age>""" % (row[0], row[1], row[2], row[3])



csvFilePath = 'sample.csv'
xmlFilePath = 'newXMLSample.xml'

data = return_python_object(csvFilePath)


with open(xmlFilePath, "a") as f:
    xml_data = ('\n'.join([convert_xml_row(row) for row in data[1:]]))
    final_xml_data = '<data>' + xml_data + '\n</data>'
    f.write(final_xml_data)
